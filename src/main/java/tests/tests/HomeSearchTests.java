package tests.tests;

import base.BaseUITest;
import driver.WebUtils;
import io.qameta.allure.Description;
import listner.Retry;
import org.testng.Assert;
import org.testng.annotations.*;
import tests.pages.AdvanceSearchPage;
import tests.pages.HomePage;

import java.io.IOException;
import java.util.List;

public class HomeSearchTests extends BaseUITest {
    private HomePage homePage;

    static final String BETWEEN = "between";
    static final String IN = "in";

    @BeforeClass(alwaysRun = true)
    public void beforeClass() {
        homePage = new HomePage(wd).navigateTo();
    }

    @AfterClass()
    public void afterTests() {
        wd.quit();
    }

    @BeforeMethod
    public void setUp() {
        WebUtils.waitForPageLoad(wd);

        Assert.assertTrue(homePage.isHomePageLoaded());
    }

    @Test(retryAnalyzer = Retry.class)
    @Description("Accessibility to search help page")
    public void TestAccessibilityToSearchHelpPage() {

        homePage.navigateToSearchHelp();
        Assert.assertEquals(wd.getTitle(), "Search Tips - Springer");
    }

    @Test(retryAnalyzer = Retry.class)
    @Description("Accessibility to search help page")
    public void TestAccessibilityToAdvancedSearchPage() {

        AdvanceSearchPage advanceSearchPage = homePage.navigateToAdvanceSearch();
        Assert.assertEquals(wd.getTitle(), "Advanced Search - Springer");

        Assert.assertTrue(advanceSearchPage.isAdvanceSearchPageLoaded());
    }

    @Test(retryAnalyzer = Retry.class)
    @Description("Search the non existing article")
    public void TestSearchNonExistingArticle() {

        String titleToBeSearched = "aveera";

        homePage.searchTitle(titleToBeSearched);

        Assert.assertEquals(homePage.getNumberOfSearchResultText(), String.format("0 Result(s) for '%s'",
                titleToBeSearched));
        Assert.assertTrue(homePage.isNoResultMessageDisplayed());
    }

    @Test(retryAnalyzer = Retry.class)
    @Description("Search article from search suggestion")
    public void TestSearchArticlesFromSuggestions() {

        homePage.typeAndGetSuggestions("test");

        List<String> firstSuggestionTitleList = homePage.getSuggestionTitles();
        String firstSuggestionTitle = firstSuggestionTitleList.get(0);
        homePage.searchTitle(firstSuggestionTitle);

        Assert.assertTrue(homePage.isTitlePresentInSearchedResults(firstSuggestionTitle));
    }

    @Test(retryAnalyzer = Retry.class)
    @Description("Search article from search suggestion clear and search again")
    public void TestSearchArticlesClearAndSearchAgain() {

        homePage.typeAndGetSuggestions("test");

        List<String> suggestionTitleList = homePage.getSuggestionTitles();
        String firstSuggestionTitle = suggestionTitleList.get(0);
        homePage.searchTitle(firstSuggestionTitle);

        Assert.assertTrue(homePage.isTitlePresentInSearchedResults(firstSuggestionTitle));

        homePage.clearSearch();
        String secondSuggestionTitle = suggestionTitleList.get(1);
        homePage.searchTitle(secondSuggestionTitle);

        Assert.assertTrue(homePage.isTitlePresentInSearchedResults(secondSuggestionTitle));
    }

    @Test(retryAnalyzer = Retry.class)
    @Description("Search within the search result based on article publishing year")
    public void TestSearchByDateInSearchResult() {

        homePage.typeAndGetSuggestions("test");

        List<String> firstSuggestionTitleList = homePage.getSuggestionTitles();

        String firstSuggestionTitle = firstSuggestionTitleList.get(0);
        homePage.searchTitle(firstSuggestionTitle);
        Assert.assertTrue(homePage.isTitlePresentInSearchedResults(firstSuggestionTitle));

        homePage.expandDateSearchPanel();

        homePage.searchByDatePublished(BETWEEN, "1900", "2021");
        Assert.assertTrue(homePage.isResultsPublishingYearBetweenRange("1900", "2021",
                homePage.getPublishingYears()));

        homePage.removeFilter();

        homePage.expandDateSearchPanel();

        homePage.searchByDatePublished(IN, "2019", null);
        Assert.assertTrue(homePage.isResultsPublishingYearCorrect("2019", homePage.getPublishingYears()));
    }

    @Test(retryAnalyzer = Retry.class, dataProvider = "getData")
    @Description("Search article from search suggestion clear and search again with multiple set of data")
    public void TestSearchArticlesClearAndSearchAgainWithMultipleData(String titleNames) {

        homePage.searchTitle(titleNames);

        Assert.assertTrue(homePage.isTitlePresentInSearchedResults(titleNames));
    }

    @Test(retryAnalyzer = Retry.class, dataProvider = "getAdvancedSearchData")
    @Description("Search article from search suggestion clear and search again with multiple set of data")
    public void TestSearchAdvancedSearchWithMultipleData(String allWords, String exactPhrase, String oneOfWords,
                                                         String withoutWords, String titleContains, String autherEditor,
                                                         String startYear, String endYear, String previewOnly) {

        AdvanceSearchPage advanceSearchPage = homePage.navigateToAdvanceSearch();
        Assert.assertEquals(wd.getTitle(), "Advanced Search - Springer");

        Assert.assertTrue(advanceSearchPage.isAdvanceSearchPageLoaded());

        advanceSearchPage.makeAdvanceSearch(allWords, exactPhrase,oneOfWords, withoutWords, titleContains, autherEditor,
                startYear, endYear,previewOnly);

        Assert.assertTrue(homePage.isResultPageDisplayed() && !homePage.isNoResultMessageDisplayed(),
                "Result is not displayed");

        homePage.clearSearch();
    }

    @DataProvider(name = "getAdvancedSearchData")
    public Object[][] getAdvancedSearchData() throws IOException {
        return parseExcelDataToDataProvider("src/main/resources/advanceSearchData.xls", "Sheet1");
    }

    @DataProvider(name = "getData")
    public Object[][] getTestData() throws IOException {
        return parseExcelDataToDataProvider("src/main/resources/testTitles.xls", "Sheet1");
    }

}