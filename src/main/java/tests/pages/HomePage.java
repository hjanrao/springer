package tests.pages;

import base.BasePage;
import driver.WebUtils;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class HomePage extends BasePage<HomePage> {

    public HomePage(WebDriver driver) {
        super(driver);
    }

    @Override
    public String getURL() {
        return "/";
    }

    @FindBy(id = "query")
    private WebElement inputSearch;

    @FindBy(id = "global-search-new")
    private WebElement linkResetSearch;

    @FindBy(id = "search")
    private WebElement btnSearch;

    @FindBy(css = "button.pillow-btn.open-search-options")
    private WebElement btnSearchOptions;

    @FindBy(id = "advanced-search-link")
    private WebElement linkAdvanceSearch;

    @FindBy(id = "search-help-link")
    private WebElement linkSearchHelp;

    @FindBy(id = "ui-id-1")
    private WebElement listSuggestion;

    @FindBy(css = "ul[id='ui-id-1'] > li")
    private List<WebElement> searchSuggestions;

    @FindBy(id = "results-list")
    private WebElement searchedResultsPage;

    @FindBy(css = "h2 a.title")
    private List<WebElement> searchedResults;

    @FindBy(id = "number-of-search-results-and-search-terms")
    private WebElement numberOfSearchResult;

    @FindBy(id = "no-results-message")
    private WebElement noRestultMessage;

    @FindBy(css = "button.expander-title")
    private WebElement btnDatePublished;

    @FindBy(id = "date-facet-mode")
    private WebElement selectDateFacet;

    @FindBy(id = "start-year")
    private WebElement inputStartYear;

    @FindBy(id = "end-year")
    private WebElement inputEndYear;

    @FindBy(id = "date-facet-submit")
    private WebElement btnDateSearch;

    @FindBy(css = "span.enumeration span.year")
    private List<WebElement> publishingYears;

    @FindBy(css = "a[title='Remove this filter'")
    private WebElement linkRemoveFilter;


    public boolean isHomePageLoaded() {

        List<WebElement> allElements = new ArrayList<>();

        allElements.add(inputSearch);
        allElements.add(btnSearch);

        try {
            WebUtils.waitForElementsToBeDisplayed(wd, allElements, 30);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public void searchTitle(String titleToBeSearched) {

        WebUtils.fill(inputSearch, titleToBeSearched);

        WebUtils.clickWithWaitForElement(wd, btnSearch, 30);
    }

    public void typeAndGetSuggestions(String titleToBeSearched) {
        WebUtils.fill(inputSearch, titleToBeSearched);
        WebUtils.waitForElementToBeDisplayed(wd, listSuggestion, 30);
    }

    public List<String> getSuggestionTitles() {
        return searchSuggestions.stream()
                .map(WebElement::getText)
                .collect(Collectors.toList());
    }

    public boolean isTitlePresentInSearchedResults(String searchedTitle) {
        List<WebElement> matchingTitles = searchedResults.stream()
                .filter(element -> element.getText().equalsIgnoreCase(searchedTitle))
                .collect(Collectors.toList());

        return matchingTitles.size() > 0;
    }

    public void clearSearch(){
        WebUtils.clickWithWaitForElement(wd, linkResetSearch, 30);
    }

    public String getNumberOfSearchResultText(){
        return WebUtils.getTextValue(wd, numberOfSearchResult, 30);
    }

    public boolean isNoResultMessageDisplayed(){
        return WebUtils.isElementDisplayed(noRestultMessage);
    }

    public void  expandDateSearchPanel(){
        WebUtils.clickWithWaitForElement(wd, btnDatePublished, 30);
    }

    public void searchByDatePublished(String dateFacet, String startYear, String endYear){

        WebUtils.waitForElementToBeDisplayed(wd,selectDateFacet, 30 );

        Select dateFacetDropdown = new Select(selectDateFacet);
        dateFacetDropdown.selectByVisibleText(dateFacet);


        if(startYear != null){
            WebUtils.waitForElementToBeDisplayed(wd,inputStartYear, 30 );
            WebUtils.fill(inputStartYear, startYear);
        }

        if(endYear != null){
            WebUtils.waitForElementToBeDisplayed(wd,inputEndYear, 30 );
            WebUtils.fill(inputEndYear, endYear);
        }

        WebUtils.clickWithWaitForElement(wd, btnDateSearch, 30);
    }

    public List<Integer> getPublishingYears() {
        return publishingYears.stream()
                .map(WebElement -> Integer.parseInt(WebElement.getText().replaceAll("^.|.$", "")))
                .collect(Collectors.toList());
    }

    public boolean isResultsPublishingYearBetweenRange(String startYear, String endYear, List<Integer> resultYear){

        int startYearValue = Integer.parseInt(startYear);
        int endYearValue = Integer.parseInt(endYear);

        List<Integer> collect = resultYear.stream()
                .filter(year -> year <= startYearValue && year >= endYearValue)
                .collect(Collectors.toList());

        return collect.size() == 0? true:false;
    }

    public boolean isResultsPublishingYearCorrect(String startYear, List<Integer> resultYear){

        int startYearValue = Integer.parseInt(startYear);

        List<Integer> collect = resultYear.stream()
                .filter(year -> year != startYearValue)
                .collect(Collectors.toList());

        return collect.size() == 0? true:false;
    }

    public void removeFilter() {
        WebUtils.clickWithWaitForElement(wd, linkRemoveFilter, 30);
    }

    public void navigateToSearchHelp() {
        WebUtils.clickWithWaitForElement(wd, btnSearchOptions, 30);
        WebUtils.clickWithWaitForElement(wd, linkSearchHelp, 30);
    }

    public AdvanceSearchPage navigateToAdvanceSearch() {
        WebUtils.clickWithWaitForElement(wd, btnSearchOptions, 30);
        WebUtils.clickWithWaitForElement(wd, linkAdvanceSearch, 30);

        return new AdvanceSearchPage(wd);
    }

    public boolean isResultPageDisplayed(){

        WebUtils.waitForElementToBeDisplayed(wd, searchedResultsPage, 30);
        return WebUtils.isElementDisplayed(searchedResultsPage);
    }
}
