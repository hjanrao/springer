package tests.pages;

import base.BasePage;
import driver.WebUtils;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class AdvanceSearchPage extends BasePage<AdvanceSearchPage> {

    public AdvanceSearchPage(WebDriver driver) {
        super(driver);
    }

    @Override
    public String getURL() {
        return "/advanced-search";
    }

    @FindBy(id = "all-words")
    private WebElement inputAllWords;

    @FindBy(id = "exact-phrase")
    private WebElement inputExactPhrase;

    @FindBy(id = "least-words")
    private WebElement inputLeastWords;

    @FindBy(id = "without-words")
    private WebElement inputWithoutWords;

    @FindBy(id = "title-is")
    private WebElement inputTitleIs;

    @FindBy(id = "author-is")
    private WebElement inputAutherIs;

    @FindBy(id = "date-facet-mode")
    private WebElement selectDateFacet;

    @FindBy(id = "facet-start-year")
    private WebElement inputStartYear;

    @FindBy(id = "facet-end-year")
    private WebElement inputEndYear;

    @FindBy(id = "results-only-access-checkbox-advanced")
    private WebElement inputResultOnly;

    @FindBy(id = "submit-advanced-search")
    private WebElement btnSubmit;

    public boolean isAdvanceSearchPageLoaded() {

        List<WebElement> allElements = new ArrayList<>();

        allElements.add(inputAllWords);
        allElements.add(inputLeastWords);
        allElements.add(inputResultOnly);
        allElements.add(btnSubmit);

        try {
            WebUtils.waitForElementsToBeDisplayed(wd, allElements, 30);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public void makeAdvanceSearch(String allWords, String exactPhrase, String oneOfWords, String withoutWords, String titleContains,
                                  String autherEditor, String startYear, String endYear, String previewOnly){

        WebUtils.fill(inputAllWords,allWords);
        WebUtils.fill(inputExactPhrase,exactPhrase);
        WebUtils.fill(inputLeastWords,oneOfWords);
        WebUtils.fill(inputWithoutWords,withoutWords);
        WebUtils.fill(inputTitleIs,titleContains);
        WebUtils.fill(inputAutherIs,autherEditor);
        WebUtils.fill(inputStartYear,startYear);
        WebUtils.fill(inputEndYear,endYear);

        if(!previewOnly.equalsIgnoreCase("yes"))
            WebUtils.clickWithWaitForElement(wd,inputResultOnly, 30);

        WebUtils.scrollToElement(wd, btnSubmit);
        WebUtils.clickWithWaitForElement(wd, btnSubmit, 30);
    }
}
