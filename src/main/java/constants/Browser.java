package constants;

public enum Browser {
    FIREFOX,
    CHROME,
    EDGE
}
